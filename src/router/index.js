import Vue from 'vue'
import Router from 'vue-router'
import Discover from '@/pages/Discover'
import HistoryP from '@/pages/History'
import Search from '@/pages/Search'
import Movie from '@/pages/Movie'
import Play from '@/pages/Play'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Discover',
      component: Discover
    },
    {
      path: '/history',
      name: 'History',
      component: HistoryP
    },
    {
      path: '/search',
      name: 'Search',
      component: Search
    },
    {
      path: '/search/:genre',
      name: 'SearchByGenre',
      component: Search
    },
    {
      path: '/movie/:id',
      name: 'Movie',
      component: Movie
    },
    {
      path: '/movie/:id/play',
      name: 'Play',
      component: Play
    }
  ]
})

export default router
