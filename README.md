# madafakaflix

> This scaffold uses Webpack, VueJS, Bootstrap, jQuery and api.tvmaze.co to list shows

_*NOTICE* **not production ready**_

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

